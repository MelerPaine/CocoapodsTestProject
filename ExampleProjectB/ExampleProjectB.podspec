Pod::Spec.new do |s|
  s.name             = 'ExampleProjectB'
  s.module_name = 'ExampleProjectB'
  s.version          = '1.0.2'
  s.summary          = 'ExampleProjectB'
  s.homepage         = 'https://gitlab.com/MelerPaine/CocoapodsTestProject.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Meler Paine' => 'pmtnmd@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/MelerPaine/CocoapodsTestProject.git', :tag => '0.0.6' }
  s.platform = :ios, '9.0'
  
  s.ios.deployment_target = '9.0'
  s.source_files  = 'ExampleProjectB/Sources/**/*.{swift}'
end
